<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="fileshare.css" />
        <title>File share </title>
    </head>
    <body>
        <?php
        session_start();
        $username = $_SESSION['username'];
        ?>
	<table class="top">
            <tr>
            <td colspan="1">
            <h1>Module2:FileSharing</h1>
            </td>
            </tr>
	    <tr>
		<td>
		<div class="account">
		<?php
		    echo "Welcome, ". $username;
		?>
		<form action="loginORout.php" method="get">
		<input type="submit" class= "logout" name="logon" value="Log Out"/>
		</form>
		</div>
		</td>
	    </tr>
	    
            <tr>
            <td>
            <div class="p1">
	<?php
        $files = scandir(sprintf("/home/fileshare/%s", $username));
        foreach ($files as $file){
            if($file === '.' || $file === '..'){continue;}
            echo sprintf('<form action="download.php" method="get"><input type="hidden" value=%s name="downloadedfile" /> <label> %s</label> <input type="submit" name="download" value="View" /> <input type="submit" name="delete" value="Delete"/> </form>', htmlentities($file), htmlentities($file));
        }

        ?>
        <form enctype="multipart/form-data" action="upload.php" method="POST">
	<p>
	    <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
	    <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
	    <input type="submit" value="Upload File" />
	</p>
        </form>
        	
            </div>
            </td>
            </tr>
        </table>

    </body>
</html>