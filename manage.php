<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>File share </title>
    </head>
    <body>
        <?php
            session_start();
            function deleteLine($deletename){
                $lines = file("/home/fileshare/users.txt");
                $output = '';
                
                foreach ($lines as $line){
                    if (!strstr(trim($line), $deletename)){
                        $output .= $line;
                    }
                }
                
                file_put_contents("/home/fileshare/users.txt",$output);
            }
            function deleteDir($dirPath) {
                    if (! is_dir($dirPath)) {
                        throw new InvalidArgumentException("$dirPath must be a directory");
                    }
                    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                        $dirPath .= '/';
                    }
                    $files = glob($dirPath . '*', GLOB_MARK);
                    foreach ($files as $file) {
                        if (is_dir($file)) {
                            self::deleteDir($file);
                        } else {
                            unlink($file);
                        }
                    }
                    rmdir($dirPath);
            }
            if(isset($_GET['delete'])){
                $name = trim($_GET['user']);
                //echo "$name";
                deleteLine($name);
                
                // this deletes the directory name of that user
                deleteDir("/home/fileshare/".$name);
                header("Location: fileshareadmin.php");
                exit;
            }
                
                
        ?>
    </body>
</html>