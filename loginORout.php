<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
            if(isset($_POST['logon']) && $_POST['logon']  == "yes"){
                $username = trim($_POST['username']);
                $checkfile = fopen("/home/fileshare/users.txt", "r") or die("can't open file");
                while( !feof($checkfile)){
                    $name = trim(fgets($checkfile));
                    if($username == $name){
                        session_start();
                        $_SESSION['username'] = $username;
                        header("Location: fileshare.php");
                        exit;
                    }
                }
                echo "Sorry this user does not exist. Please try again.";
            }
            elseif(isset($_POST['newuser'])){
                $newuser = $_POST['newuser'];
                $checkfile = fopen("/home/fileshare/users.txt", "r") or die("can't open file");
                while( !feof($checkfile)){
                    $name = trim(fgets($checkfile));
                    if($name == $newuser){
                        echo "This user already exists! Please go back and enter a different name";
                        exit;
                    }
                }
                $newfile = fopen("/home/fileshare/users.txt", "a+");
                fwrite($newfile, $newuser."\n");
                fclose($newfile);
                session_start();
                $newdirpath = "/home/fileshare/".$newuser;
                mkdir($newdirpath);
                //echo "$newdirpath";
                echo "this should have added a new directory and line in text file";
                $_SESSION['username'] = $newuser;
                header("Location: fileshare.php");
                exit;
            }
            else{
                session_destroy();
                header("Location: fileshare.html");
            }
            
    ?>    
    </body>
</html>
