<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
            session_start();
            $filename = $_GET['downloadedfile'];
            $username = $_SESSION['username'];
            if ($_GET['download'] == null){
                $full_path = sprintf("/home/fileshare/%s/%s", $username, $filename);
                unlink($full_path);
                header("Location: fileshare.php");
                exit;
            }
            else{
                // We need to make sure that the filename is in a valid format; if it's not, display an error and leave the script.
                // To perform the check, we will use a regular expression.
                if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
                        echo "Invalid filename";
                        exit;
                }
                 
                // Get the username and make sure that it is alphanumeric with limited other characters.
                // You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
                // since we will be concatenating the string to load files from the filesystem.
                
                if( !preg_match('/^[\w_\-]+$/', $username) ){
                        echo "Invalid username";
                        exit;
                }
                 
                $full_path = sprintf("/home/fileshare/%s/%s", $username, $filename);
                 
                // Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $mime = $finfo->file($full_path);
                 
                // Finally, set the Content-Type header to the MIME type of the file, and display the file.
                header("Content-Type: ".$mime);
                readfile($full_path);
                
            }
        ?> 
    </body>
</html>


