<!DOCTYPE HTML>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>File share </title>
    </head>
<?php
    session_start();
    // Get the filename and make sure it is valid
    $filename = basename($_FILES['uploadedfile']['name']);
    if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            exit;
    }
     
    // Get the username and make sure it is valid
    $username = $_SESSION['username'];
    if( !preg_match('/^[\w_\-]+$/', $username) ){
            echo "Invalid username";
            exit;
    }
     
    $full_path = sprintf("/home/fileshare/%s/%s", $username, $filename);
     
    if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
            header("Location: fileshare.php");
            exit;
    }else{
            echo "Didn't work";
            //header("Location: upload_failure.htm");
            //exit;
    }
?>